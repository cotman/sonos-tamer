#!/usr/bin/env python2
"""
Sonos Tamer

Runs continuously, checking every CHECK_INTERVAL seconds, and drops the volume by 1 (if required).

"""


import datetime
import soco
import threading
import time

ACCEPTABLE_VOLUMES = {
    "kitchen": 7,
    "reception": 14
}

CHECK_INTERVAL = 60
TAMED_HOUR_START = 8
TAMED_HOUR_END = 16

def log_output(text):
    """
    Timed output log.
    """

    print "{}: {}".format(time.ctime(), text)

def check_sonos_volume():
    """
    Check the volume of Sonos players on the local network - adjust volume limited ones accordingly.
    """

    if datetime.datetime.now().hour not in range(TAMED_HOUR_START, TAMED_HOUR_END):
        log_output("Not taming Sonos as outside hour range {} to {}".format(
            TAMED_HOUR_START,
            TAMED_HOUR_END
        ))
        threading.Timer(CHECK_INTERVAL, check_sonos_volume).start()
        return

    try:
        zones = soco.discover()

        if zones is None:
            log_output("Can't detect Sonos zones on this run")
            threading.Timer(CHECK_INTERVAL, check_sonos_volume).start()
            return

        for zone in zones:
            log_output("Found Sonos '{}'".format(zone.player_name))
            acceptable_volume = ACCEPTABLE_VOLUMES.get(zone.player_name)

            if acceptable_volume:
                log_output("\tPlayer is volume monitored - current volume: '{}', acceptable volume: '{}'".format(
                    zone.volume,
                    acceptable_volume
                ))
                if zone.volume > acceptable_volume:
                    new_volume = zone.volume - 1
                    log_output("\tDropping volume to '{}' on this run".format(new_volume))
                    zone.volume = new_volume
                else:
                    log_output("\tVolume is acceptable")
    except:
        log_output("Error querying or updating Sonos zones on this run")

    threading.Timer(CHECK_INTERVAL, check_sonos_volume).start()

if __name__ == "__main__":
    """
    Entry point.
    """

    try:
        check_sonos_volume()
    except Exception as exception:
        print "Error: Problem running script - '{}'.".format(exception.message)
