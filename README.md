# Sonos Tamer

Having problems with one or more office Sonos disturbing you doing actual work?

Just adjust the "acceptable" values at the top of this script for the players
on your network, and set it running in a screen session.

It drops the volume by `1` every `CHECK_INTERVAL` seconds, so the selfish
folk that haven't asked you whether it's even OK to have music on probably
won't even notice.

    mkvirtualenv sonos
    pip install -r requirements.txt
    ./sonos_tamer.py

...and relax again.

